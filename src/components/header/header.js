import reactLogo from "../../assets/images/react-logo.svg"

function Header() {
    return (
        <div className="header">
            <div className="container text-center">
            <div className="logo">
                <a href="/">
                <img src={reactLogo} alt="React Store" width={48}/>
                </a>
            </div>
            <h1>React Store</h1>
            <p>Demo App Shop24h v1.0</p>
            </div>
        </div>
    )
}

export default Header