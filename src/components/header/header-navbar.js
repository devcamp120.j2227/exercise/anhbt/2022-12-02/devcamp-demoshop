function HeaderNavbar () {
    return <nav className="navbar bg-dark navbar-dark">
        <div className='container'>
          <ul className="navbar-nav mr-auto">
            <li className="nav-item"><a aria-current="page" className="nav-link active" href="/">Home</a></li>
          </ul>
        </div>
      </nav>
}

export default HeaderNavbar