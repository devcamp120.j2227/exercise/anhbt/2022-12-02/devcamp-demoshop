import ProductData from "../../assets/data/ProductData.json"
let ProductsList = ProductData.Products.slice(0,9)
function ProductCards (paramProduct, paramIndex) {
    return(
    <div key={"product" + paramIndex} className="product-box card bg-light mb-3">
        <div className="card-header">
            <h5 className="card-title">
            <a className="text-decoration-none" href="/">{paramProduct.Title}</a>
            </h5>
        </div>
        <div className="card-body">
            <div className="text-center">
            <a href="/product-detail/1">
                <img className="card-img-top" alt={paramProduct.Title} src={paramProduct.ImageUrl} />
            </a>
            </div>
            <p className="card-text description">{paramProduct.Description}</p>
            <p className="card-text">
            <b>Category:</b> {paramProduct.Category}
            </p>
            <p className="card-text">
            <b>Made by:</b> {paramProduct.Manufacturer}
            </p>
            <p className="card-text">
            <b>Organic:</b> {paramProduct.Organic ? "Yes" : "No"}
            </p>
            <p className="card-text">
            <b>Price:</b> ${paramProduct.Organic ? paramProduct.OrgPrice : paramProduct.Price}
            </p>
            <div className="add-cart-container">
            <button type="button" className="btn btn-primary">Add to Cart</button>
            </div>
        </div>
    </div>
    )
}
function Content() {
    return (
        <div className='container main-container'>
            <div>
                <h3>Product List</h3>
                <div>
                    <p>Showing 1 - 9 of 24 products</p>
                </div>
            </div>
            <div className="product-container">
                {ProductsList.map(function(product, index){
                    return ProductCards(product, index);
                })}
            </div>
        </div>
    )
}
export default Content;