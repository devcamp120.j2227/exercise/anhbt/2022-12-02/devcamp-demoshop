import './App.css';
import Header from "./components/header/header"
import HeaderNavbar from './components/header/header-navbar';
import Content from './components/content/content';
import Footer from './components/footer/footer';
function App() {
  return (
    <div className="App">
      <Header></Header>
      <HeaderNavbar></HeaderNavbar>
      <Content></Content>
      <Footer></Footer>
    </div>
  );
}

export default App;
